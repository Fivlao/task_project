#ifndef TESTLIBCHILD_H
#define TESTLIBCHILD_H

#include "testlibinterface.h"

class TESTLIB_EXPORT TestlibChild: public TestlibInterface
{
    Q_OBJECT
public:
    TestlibChild(QObject *parent = nullptr);

public slots:
    int dosmth(const int param) const override;
};

#endif // TESTLIBCHILD_H
