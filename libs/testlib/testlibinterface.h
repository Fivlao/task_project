#ifndef TESTLIBINTERFACE_H
#define TESTLIBINTERFACE_H

#include "testlib_global.h"

#include <QObject>

class TESTLIB_EXPORT TestlibInterface: public QObject
{
    Q_OBJECT
public:
    TestlibInterface(QObject *parent = nullptr): QObject(parent) {};
    virtual ~TestlibInterface() {};
    virtual int dosmth(int param) const = 0;

};

#endif // TESTLIBINTERFACE_H

