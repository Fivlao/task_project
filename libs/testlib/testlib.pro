QT -= gui
QT += core

TEMPLATE = lib
TARGET = testlib
DEFINES += TESTLIB_LIBRARY

CONFIG += shared dll
CONFIG += c++17

DESTDIR = $$OUT_PWD\..\..\..\bin\libs\testlib

SOURCES += \
    testlibchild.cpp \
    testlibinterface.cpp

HEADERS += \
    testlib_global.h \
    testlibchild.h \
    testlibinterface.h

lib_default.files = \
    testlib_global.h \
    testlibchild.h \
    testlibinterface.h
lib_default.path = $$OUT_PWD\..\..\..\bin\libs\testlib

COPIES += lib_default

!isEmpty(target.path): INSTALLS += target
