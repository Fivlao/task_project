import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import Testlib 1.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    property TestlibChild testlibChild: TestlibChild {}

    property QtObject colorSchema: QtObject {
        readonly property color ligthСolor: "springgreen"
        readonly property color mediumСolor: "darkslategrey"
        readonly property color darkColor: "#003049"
    }

    Rectangle {
        anchors.fill: parent
        color: colorSchema.darkColor

        ColumnLayout {
            anchors.fill: parent
            spacing: 5

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
            }

            Label {
                id: textItem
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: 300
                Layout.preferredHeight: 40
                color: colorSchema.ligthСolor
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                text: "0"
                font.pointSize: 11
                background: Rectangle {
                    anchors.fill: parent
                    color: colorSchema.mediumСolor
                    radius: 5

                    Rectangle {
                        width: parent.width -10
                        anchors.bottom: parent.bottom
                        anchors.right: parent.right
                        anchors.left: parent.left
                        anchors.bottomMargin: 5
                        anchors.rightMargin: 5
                        anchors.leftMargin: 5
                        radius: 1
                        height: 2
                        color: colorSchema.ligthСolor
                    }
                }
            }

            SpinBox {
                id: spinBox
                Layout.preferredWidth: 300
                Layout.alignment: Qt.AlignCenter
                Layout.preferredHeight: 40
                editable: true
                from: -999
                value: 0
                to: 999
            }

            Button {
                Layout.preferredWidth: 300
                Layout.alignment: Qt.AlignCenter
                Layout.preferredHeight: 40

                background: Rectangle {
                    color: "transparent"
                }

                contentItem: Rectangle {
                    anchors.fill: parent
                    color: colorSchema.ligthСolor
                    radius: 5

                    Text {
                        anchors.fill: parent
                        text: "Порахувати"
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: 11
                        color: colorSchema.mediumСolor
                    }
                }
                onClicked: textItem.text = testlibChild.dosmth(spinBox.value)
            }

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }
    }
}
